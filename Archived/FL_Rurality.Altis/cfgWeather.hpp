class cfgWeather {
	overcastRandomize = 1;
	overcastFrame[] = {0.45,1}; // [0,1]

	rainRandomize = 1;
	rainFrame[] = {0,0.3}; // [0,1]

	fogRandomize = 1;
	fogFrame[] = {0.025,0.15}; // [0,1]

	timeRandomize = 1;
	timeFrame[] = {7, 18}; // [0,24]
};
