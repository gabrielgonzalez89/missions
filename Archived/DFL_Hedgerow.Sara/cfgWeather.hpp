class CfgWeather {
	overcastRandomize = 1;
	overcastFrame[] = {0.8,0.9}; // [0,1]

	rainRandomize = 0;
	rainFrame[] = {0,1}; // [0,1]

	fogRandomize = 1;
	fogFrame[] = {0,0.09}; // [0,1]

	timeRandomize = 1;
	timeFrame[] = {6,18}; // [0,24]
};