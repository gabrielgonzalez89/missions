class Sides {
    class West {
        name = "NATO";
        squads = "Apex_NATO";
        playerClass = "FRL_B_Soldier_USA_D_F";
        flag = "a3\Data_f\cfgFactionClasses_BLU_ca.paa";
        mapIcon = "a3\ui_f\data\Map\Markers\NATO\b_installation.paa";
        color[] = {0, 0.3, 0.8, 1};
        squadRallyPointObject = "FRL_Backpacks_West";
        FOBObjects[] = {{"FRL_FO_Box1", {0,0,0}, 0}, {"Land_SatelliteAntenna_01_F", {0,0.22,0}, 177}};
    };

    class East : West {
        name = "PRC";
        squads = "Apex_China";
        playerClass = "O_Soldier_FRL_CHN_07Woodland";
        flag = "pr\frl\addons\uniforms\factions\Cfgfactionclasses_chn_pla.paa";
        mapIcon = "a3\ui_f\data\Map\Markers\NATO\o_installation.paa";
        color[] = {0.5, 0, 0, 1};
        squadRallyPointObject = "FRL_Backpacks_East";
        FOBObjects[] = {{"FRL_FO_Box1", {0,0,0}, 0}, {"Land_SatelliteAntenna_01_F", {0,0.22,0}, 177}};
    };
};
