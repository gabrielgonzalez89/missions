class cfgFactions {
    class West {
        factionClass = "RHS_Iraq";
    };

    class East : West {
        factionClass = "RHS_INS_D";
    };
};
