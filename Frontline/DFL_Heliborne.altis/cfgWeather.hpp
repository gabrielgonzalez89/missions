class CfgWeather {
	overcastRandomize = 1;
	overcastFrame[] = {0,0.3}; // [0,1]

	rainRandomize = 0;
	rainFrame[] = {0,1}; // [0,1]

	fogRandomize = 0;
	fogFrame[] = {0,0.09}; // [0,1]

	timeRandomize = 1;
	timeFrame[] = {2,19}; // [0,24]
};