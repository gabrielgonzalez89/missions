class CfgSectors {
    class base_west {
        designator = "HQ"; // Name in spawn list
        sectortype = "mainbase"; // Mainbase to allow spawning
        aiSpawnAllow = 1; // 0 to prevent AI spawning here
        aiSpawnOnly = 0; // 1 to only allow AI spawning here
        //spawnMarker = "baseSpawn_west_1"; // Different marker than  middle
        //spawnHeight = 15; // Height above sea/terrain
    };

    class base_east {
        designator = "HQ"; // Name in spawn list
        sectortype = "mainbase"; // Mainbase to allow spawning
        aiSpawnAllow = 1; // 0 to prevent AI spawning here
        aiSpawnOnly = 0; // 1 to only allow AI spawning here
        //spawnMarker = "baseSpawn_west_1"; // Different marker than  middle
        //spawnHeight = 15; // Height above sea/terrain
    };
};
