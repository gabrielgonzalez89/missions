class cfgFactions {
    class West {
        factionClass = "RHS_UN";
    };

    class East : West {
        factionClass = "RHS_INS_D";
    };
};
