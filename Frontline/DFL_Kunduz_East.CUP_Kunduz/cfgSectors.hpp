class CfgSectors {
    class base_west {
        designator = "HQ";
        sectortype = "mainbase";
    };
	
    class base_west_1 {
        designator = "East Base";
        sectortype = "mainbase";
        spawnMarker = "baseSpawn_west_1";
    };

    class base_east {
        designator = "HQ";
        sectortype = "mainbase";
    };
	
    class base_east_1 {
        designator = "East Base";
        sectortype = "mainbase";
        spawnMarker = "baseSpawn_east_1";
    };
};
