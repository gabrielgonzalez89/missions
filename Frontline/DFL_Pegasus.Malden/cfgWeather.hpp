class CfgWeather {
	overcastRandomize = 1;
	overcastFrame[] = {0,0.5}; // [0,1]

	rainRandomize = 1;
	rainFrame[] = {0,1}; // [0,1]

	fogRandomize = 1;
	fogFrame[] = {0,0.09}; // [0,1]

	timeRandomize = 1;
	timeFrame[] = {3,23}; // [0,24]
};
