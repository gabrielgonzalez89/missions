class cfgFactions {
    class West {
        factionClass = "RHS_USA_W";
    };

    class East : West {
        factionClass = "RHS_RUA_W";
    };
};
