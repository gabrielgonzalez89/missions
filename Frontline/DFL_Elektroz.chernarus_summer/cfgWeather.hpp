class cfgWeather {
	overcastRandomize = 1;
	overcastFrame[] = {0,0.5}; // [0,1]

	rainRandomize = 0;
	rainFrame[] = {0,1}; // [0,1]

	fogRandomize = 1;
	fogFrame[] = {0,0.1}; // [0,1]

	timeRandomize = 1;
	timeFrame[] = {6,18}; // [0,24]
};