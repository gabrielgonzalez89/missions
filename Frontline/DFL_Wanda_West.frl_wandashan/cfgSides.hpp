class cfgFactions {
    class West {
        factionClass = "RHS_RUA_W_Blu";
    };

    class East : West {
        factionClass = "RHS_INS_W";
    };
};
