tickets = 200; // number of tickets per team
musicStart = 20; // tickets to start the music
playerTicketValue = 1; // Ticket value of a player

// Optional ConfigValue used if no Near Location can be found
markerLocation[] = {
    "base_east",
    "base_west",
    "sector_0",
    "sector_1",
    "sector_2",
    "sector_3",
    "sector_4",
    "sector_5",
    "sector_6",
    "sector_7",
    "sector_8",
    "sector_9",
    "sector_10",
    "sector_11",
    "sector_12",
    "sector_13"
};
