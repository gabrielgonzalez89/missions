class Sides {
    class West {
        name = "USA";
        playerClass = "FRL_B_Soldier_USMC_F";
        flag = "\rhsusf\addons\rhsusf_main\data\armylogo.paa";
        mapIcon = "a3\ui_f\data\Map\Markers\NATO\b_installation.paa";
        color[] = {0, 0.3, 0.8, 1};
        squadRallyPointObject = "FRL_Backpacks_West";
        FOBObjects[] = {{"FRL_FO_Box1", {0,0,0}, 0}, {"Land_SatelliteAntenna_01_F", {0,0.22,0}, 177}};
        squads = "RHS_USA_Woodland";

    };

    class East : West {
        name = "RU";
        playerClass = "FRL_O_Soldier_RUA_W_F";
        flag = "\rhsafrf\addons\rhs_main\data\icons\msv.paa";
        mapIcon = "a3\ui_f\data\Map\Markers\NATO\o_installation.paa";
        color[] = {0.5, 0, 0, 1};
        squadRallyPointObject = "FRL_Backpacks_East";
        FOBObjects[] = {{"FRL_FO_Box1", {0,0,0}, 0}, {"Land_SatelliteAntenna_01_F", {0,0.22,0}, 177}};
        squads = "RHS_RUA_Woodland";

    };
};
